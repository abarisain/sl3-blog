Title: Présentation du SL3
Date: 2013-01-10 18:00:00


SUPINFO Low-Level Laboratory ou SL3 est un projet étudiant autour d'une passion commune : l'informatique. La philosophie du SL3 est de rassembler les étudiants autour de problématiques de bas-niveau. Que l'on soit développeur ou simples curieux, nous sommes utilisateurs de technologies performantes mais de plus en plus abstraites à nos yeux. Notre but est de comprendre et d'assimiler ce que nous utilisons chaque jour en tant que passioné d'informatique. 


## Présentation

<img style="float: right; padding: 5px;" src="http://i.imgur.com/eKYaI.png" "BSD in 1971" />

Plus concrètement, les thèmes abordés ici sont par exemple les **systèmes d'exploitation**. Tous les jours nous utilisons un système d'exploitation mais savons-nous comment il marche ? Que se passe-t'il quand un système d'exploitation démarre ? Comment fonctionne le noyau de mon système d'exploitation ? Est-ce que c'est facile à programmer ? Voici le genre de question que nous nous posons et auxquelles nous voudrions apporter une réponse.

<img style="float: left; padding: 5px;" src="http://i.imgur.com/jEiE8.png" alt="Linux kernel" />

Etant très attachés au **développement**, nous nous intéressons évidemment à tout ce qui gravite autour. Nous portons un intérêt certain pour les langages informatiques, la compilation, la conception et l'algorithmique. Nous nous intéressons aussi à la **programmation parallèle et concurrente** afin de comprendre ses enjeux. Bref, tout ce qui tourne autour du développement et qui constitue sa base, cette base que nous voulons mieux assimiler.

Nos objectifs majeurs sont la recherche et le développement, la recherche d'une part pour comprendre les domaines sus-cités et le développement d'autre part pour assimiler de nouvelles problématiques et répondre par le code car après tout, la pratique est le meilleur moyen d'apprendre. Le but final est de découvrir de nouveaux horizons que de se perfectionner les acquis.


## Projet commun

Afin de justement nous intéresser à des problèmatiques de bas-niveau tout en nous entrainant à coder, nous avons décidé de mener un projet de machine virtuelle logicielle à l'instar de la [JVM](http://fr.wikipedia.org/wiki/Machine_virtuelle_Java). Plus d'informations à ce sujet viendront bientôt.


## Organisation

Au niveau de notre organisation, la direction est assurée par [Timothée BERNARD](mailto:timothee.bernard@supinfo.com) et [Mathieu STEFANI](mailto:mathieu.stefani@supinfo.com) présents sur le campus de Montréal (Canada). Seul campus auquel nous sommes présents à l'heure actuelle.  

Dans la mesure du possible, nous souhaiterions nous étendre sur plusieurs campus, si vous êtes étudiant à SUPINFO et que vous voulez représenter le SL3 sur votre campus, n'hésitez pas à envoyer un mail à [timothee.bernard@supinfo.com](mailto:timothee.bernard@supinfo.com).


## Communication

D'un point de vue communication et afin de rassembler les étudiants de n'importe quelle ville, nous disposons :
 
 * De [forums](http://www.lab-sl3.org/forums/).
 * D'un salon IRC : #sl3@irc.supnetwork.org.
 * D'un subreddit : [/r/sl3](http://www.reddit.com/r/sl3) pour partager les meilleurs liens en rapport avec nos activités.

Si vous êtes intéressés par ce que nous faisons ou si vous avez des questions, n'hésitez pas à passer sur les forums ou sur notre channel IRC !



Après cette présentation, je vous souhaite bonne navigation sur le site internet de notre laboratoire étudiant !
