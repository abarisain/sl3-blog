#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# Site
AUTHOR = 'SL3 Team'
SITENAME = 'SUPINFO Low-Level Laboratory'
SITEURL = 'http://www.lab-sl3.org' # no trailing slash at the end !

# Language, Date format, Timezone...
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'fr'

# Navigation
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

MENUITEMS = ((u'Accueil', SITEURL),
             (u'Présentation', '%s/pages/presentation-du-sl3.html' % SITEURL),
             (u'Forums', '%s/forums/' % SITEURL),
             (u'Reddit', 'http://www.reddit.com/r/sl3'),)

# Look
THEME = 'notmyidea-fr'

# Articles
DEFAULT_CATEGORY = 'news'
DEFAULT_PAGINATION = 5

ARTICLE_URL = 'articles/{category}/{slug}.html'
ARTICLE_SAVE_AS = 'articles/{category}/{slug}.html'

ARTICLE_LANG_URL = 'articles/{category}/{slug}-{lang}.html'
ARTICLE_LANG_SAVE_AS = 'articles/{category}/{slug}-{lang}.html'

# Blogroll
LINKS = ()

# Static files
FILES_TO_COPY = (('extra/favicon.ico', 'favicon.ico'),)
